import pandas as pd
import matplotlib.pyplot as plt
#install package openpyxl 3.1.0


#case1
data = pd.read_excel('Case_study.xlsx',sheet_name="GDP_data")

growth=[]
for i in range(1,len(data)):
    if data.values[i][0] >= 2000 and data.values[i][0] <=2019:
        growth.append(((data.values[i][1]-data.values[i-1][1])/data.values[i-1][1])*100)
    else:
        growth.append(None)


data = pd.read_excel('Case_study.xlsx',sheet_name="Default_data")
new_growth =[]
for i in range(len(data)):
    val = int(data.values[i][1]-2000)
    new_growth.append(growth[val])

data["Growth"] = new_growth





#case 2

counted = data.In_default.value_counts()[1]
all = len(data)
default_ratio = 100 * counted/all
not_default_ratio = 100-default_ratio
labels = f'default cases {default_ratio} %',f'not default cases {not_default_ratio} %'
sizes = [counted,all-counted]
fig, ax = plt.subplots()
ax.pie(sizes, labels=labels)
plt.title("Udział przypadków narażonych na bycie 'złymi klientami' w stosunku do wszystkich przypadków")
plt.show()

bucket1 = data.loc[data["DPD"]==0]
bucket2 = data.loc[data["DPD"].isin(range(1,31))]
bucket3 = data.loc[data["DPD"].isin(range(31,61))]
bucket4 = data.loc[data["DPD"].isin(range(61,91))]

rate1 = 100 * bucket1.In_default.value_counts()[1]/len(bucket1)
rate2 = 100 * bucket2.In_default.value_counts()[1]/len(bucket2)
rate3 = 100 * bucket3.In_default.value_counts()[1]/len(bucket3)
rate4 = 100 * bucket4.In_default.value_counts()[1]/len(bucket4)

height=[rate1, rate2, rate3, rate4]
labels = [f"0 DPD \n {round(rate1,2)} %",f"1-30 DPD \n {round(rate2,2)} %", f"31-60 DPD \n {round(rate3,2)} %", f"61-90 DPD \n {round(rate4,2)} %"]

plt.bar(labels,height)
plt.title("Procentowy odsetek klientów narażonych na 'bycie złym' w zależnośći od przedziału DPD")
plt.show()



h=[]
l=[]
all = data.In_default.value_counts()[1]

for i in range(2000,2020):
    tmp  = data.loc[data["Reporting_date"]==i]
    h.append(100 * tmp.In_default.value_counts()[1]/all)
    l.append(f" {i}")

fig, ax = plt.subplots()
ax.bar(l,h)
plt.title("Rozkład złych przypadków w poszczególnych latach")


fig, ax = plt.subplots()
ax.bar(l,growth)
plt.title("Współczynnik PKB w danych latach")
plt.show()

h=[]
l=[]

for i in range(10):
    a= 10*i/100
    b = a+0.1
    tmp = data.loc[data["TtM"].between(a,b)]
    h.append(100 * tmp.In_default.value_counts()[1]/all)
    l.append(f" {i} \n {10*i} - {10*(i+1)} %")

fig, ax = plt.subplots()
ax.bar(l,h)
plt.title("Rozkład złych przypadków w zależności od TtM")
plt.show()

# case 3
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report

dataset = data[["DPD","Growth","In_default"]]
dataset.head()

X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 2].values
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.10)
scaler = StandardScaler()
scaler.fit(X_train)


X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)

#KNN
classifier = KNeighborsClassifier(n_neighbors=5)
classifier.fit(X_train, y_train)
y_predict = classifier.predict(X_test)

print(classification_report(y_test, y_predict))


# aby uzyskać prawdopodobieństwo zamiast klasyfikacji w 3 poniższych algorytmach zamiast
# funkcji  predict należy użyć predict_proba
#SVM - wektory nośne
from sklearn import svm
from sklearn import metrics

print("test")

clf = svm.SVC(kernel='rbf') # Linear Kernel
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)
print("SVM accuracy:",metrics.accuracy_score(y_test, y_pred))

#Naive Bayes
from sklearn.naive_bayes import GaussianNB
gnb = GaussianNB()
y_pred = gnb.fit(X_train, y_train).predict(X_test)
print("Naive Bayes accuracy:",metrics.accuracy_score(y_test, y_pred))

#Decision tree

from sklearn import tree

clf = tree.DecisionTreeClassifier()
clf = clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)
print("Decision Tree accuracy:",metrics.accuracy_score(y_test, y_pred))



#sieci neuronowe
from keras.models import Sequential
from keras.layers import Dense

model = Sequential()
model.add(Dense(128, activation='relu', input_dim=2))
model.add(Dense(1, activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
model.summary()
hist = model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=100, batch_size=100)
acc = hist.history['accuracy']
print(acc[-10:])

import tensorflow as tf
from tensorflow import keras



model = keras.Sequential([
    keras.layers.Flatten(input_shape=(2,)),
    keras.layers.Dense(16, activation=tf.nn.relu),
	keras.layers.Dense(16, activation=tf.nn.relu),
    keras.layers.Dense(1, activation=tf.nn.sigmoid),
])

model.compile(optimizer='adam',
              loss='binary_crossentropy',
              metrics=['accuracy'])

model.fit(X_train, y_train, epochs=50, batch_size=1)

test_loss, test_acc = model.evaluate(X_test, y_test)
print('Test accuracy:', test_acc)


# regresja liniowa
from sklearn.linear_model import LinearRegression

model = LinearRegression().fit(X_train, y_train)
r_sq = model.score(X_train, y_train)
y_pred = model.predict(X_test)
print(f"predicted response:\n{y_pred}")

